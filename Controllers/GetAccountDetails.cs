﻿using InstantReporting.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace InstantReporting.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GetAccountDetails
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private string ClientID = "a791a0c3-dca0-42b5-a1ce-0ecf6d07c644";
        private string ClientSecret = "D6bF6hA4wW1qT6rX0mM1gW0vJ0lJ3dD4cL7jN5pC5vF4uY1eK1";
        private string Signature = "SKIP_SIGNATURE_VALIDATION_FOR_SANDBOX";
        private string authorizerId = "70311198";
        private string currentTimeStamp = DateTime.UtcNow.ToString("ddd, dd MMM yyy HH:mm:ss 'GMT'");
        private string host = "api.nordeaopenbanking.com";
        public GetAccountDetails(ILogger<WeatherForecastController> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet]
        public async Task<IActionResult> PostAuthorize()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.nordeaopenbanking.com/corporate/v2/authorize");
             
            var initiateAuthdata = new InitiateAuthContent
            {
                agreement_number = "130474822427",
                duration = "129600",
                scope = new List<string>() { "ACCOUNTS_BROADBAND", "PAYMENTS_BROADBAND" }
            };
            var InitiateAuthBody = JsonSerializer.Serialize(initiateAuthdata);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Content = new StringContent(InitiateAuthBody, Encoding.UTF8);
            request.Headers.Add("X-IBM-Client-Id", ClientID);
            request.Headers.Add("X-IBM-Client-Secret", ClientSecret);
            request.Headers.Add("Signature", Signature);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var client = _httpClientFactory.CreateClient();

            HttpResponseMessage response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var authResponse = await response.Content.ReadAsStringAsync();
                var postAuthorizeResponse = JsonSerializer.Deserialize<AuthInitiateResponse>(authResponse);
                //return new OkObjectResult(JsonSerializer.Deserialize<AuthInitiateResponse>(authResponse));

               return new OkObjectResult(await PutAuthorize(postAuthorizeResponse.response.access_id, postAuthorizeResponse.response.client_token));

            }
            else
            {
                return new BadRequestObjectResult(response);
            }
        }

        public async Task<IActionResult> PutAuthorize(string accessId,string client_token)
        { 

            var request = new HttpRequestMessage(HttpMethod.Put, "https://api.nordeaopenbanking.com/corporate/v2/authorize/"+ accessId);
            var authID = new AuthId { authorizer_id = authorizerId };
            var authorizeId = JsonSerializer.Serialize(authID);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Content = new StringContent(authorizeId, Encoding.UTF8);
            request.Headers.Add("X-IBM-Client-Id", ClientID);
            request.Headers.Add("X-IBM-Client-Secret", ClientSecret);
            request.Headers.Add("Signature", Signature);
            request.Headers.Add("Authorization", "Bearer "+client_token);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var client = _httpClientFactory.CreateClient();

            HttpResponseMessage response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var authResponse = await response.Content.ReadAsStringAsync();
                //return new OkObjectResult(JsonSerializer.Deserialize<AuthInitiateResponse>(authResponse));
                return new OkObjectResult(await GetAuthorize(accessId, client_token));
            }
            else
            {
                return new BadRequestObjectResult(response);
            }
        }

        public async Task<IActionResult> GetAuthorize(string accessId, string client_token)
        {

            var request = new HttpRequestMessage(HttpMethod.Get, "https://api.nordeaopenbanking.com/corporate/v2/authorize/" + accessId);
            //var authID = new AuthId { authorizer_id = authorizerId };
            //var authorizeId = JsonSerializer.Serialize(authID);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //request.Content = new StringContent(authorizeId, Encoding.UTF8);
            request.Headers.Add("X-IBM-Client-Id", ClientID);
            request.Headers.Add("X-IBM-Client-Secret", ClientSecret);
            request.Headers.Add("Signature", Signature);
            request.Headers.Add("Authorization", "Bearer " + client_token);
            //request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var client = _httpClientFactory.CreateClient();

            HttpResponseMessage response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var authResponse = await response.Content.ReadAsStringAsync();
                var getAuthorizeResponse=JsonSerializer.Deserialize<AuthInitiateResponse>(authResponse);
                return new OkObjectResult(await PostAuthorizeToken(getAuthorizeResponse.response.code));
            }
            else
            {
                return new BadRequestObjectResult(response);
            }
        }

        public async Task<IActionResult> PostAuthorizeToken(string code)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.nordeaopenbanking.com/corporate/v2/authorize/token");

            request.Headers.Add("X-IBM-Client-Id", ClientID);
            request.Headers.Add("X-IBM-Client-Secret", ClientSecret);
            request.Headers.Add("Signature", Signature);
            
            var contentList = new List<string>();
            contentList.Add($"code={Uri.EscapeDataString(code)}");
            contentList.Add($"grant_type={Uri.EscapeDataString("authorization_code")}");
            request.Content = new StringContent(string.Join("&", contentList));
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            var client = _httpClientFactory.CreateClient();

            HttpResponseMessage response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var authResponse = await response.Content.ReadAsStringAsync();
                var postAuthorizetokenResponse = JsonSerializer.Deserialize<AuthInitiateResponse>(authResponse);
                return new OkObjectResult(GetAccount(postAuthorizetokenResponse.response.access_token));
                //return new OkObjectResult(postAuthorizetokenResponse);
            }
            else
            {
                return new BadRequestObjectResult(response);
            }
        }

        public async Task<IActionResult> GetAccount(string access_token)
         {

             var request = new HttpRequestMessage(HttpMethod.Get, "https://api.nordeaopenbanking.com/corporate/v2/accounts");
             request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
             
             request.Headers.Add("X-IBM-Client-Id", ClientID);
             request.Headers.Add("X-IBM-Client-Secret", ClientSecret);
             request.Headers.Add("X-Nordea-Originating-Date", currentTimeStamp);
             request.Headers.Add("X-Nordea-Originating-Host", host);
             request.Headers.Add("Signature", Signature);
             request.Headers.Add("Authorization", "Bearer " + access_token);

             var client = _httpClientFactory.CreateClient();

             HttpResponseMessage response = await client.SendAsync(request);

             if (response.IsSuccessStatusCode)
             {
                              
                var accres = new AccountResponse();

                 var getAccountResponse = await response.Content.ReadAsStringAsync();
                accres = JsonSerializer.Deserialize<AccountResponse>(getAccountResponse);
                 return new OkObjectResult(accres.response);
                 //return new OkObjectResult(getAccountResponse);
            }
             else
             {
                 return new BadRequestObjectResult(response);
             }
         }

    }

    internal class AuthId
    {
        public string authorizer_id { get; set; }
    }

    internal class InitiateAuthContent
    {
        public string agreement_number { get; set; }
        public string duration { get; set; }
        public List<string> scope { get; set; }
    }
}
