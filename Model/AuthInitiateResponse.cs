﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstantReporting.Model
{
    public class AuthInitiateResponse
    {       
        public GroupHeader group_header { get; set; }
        public AuthResponse response { get; set; }
    }

    public class AuthResponse
    {
        public string access_id { get; set; }
        public string status { get; set; }
        public string client_token { get; set; }
        public string code { get; set; }
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
        public List<Links> _links { get; set; }
        
    }

    public class Links
    {
        public string rel { get; set; }
        public string href { get; set; }
    }

    public class GroupHeader
    {
        public string message_identification { get; set; }
        public string creation_date_time { get; set; }
        public int http_code { get; set; }
    }
}
