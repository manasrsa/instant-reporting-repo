﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InstantReporting.Model
{
    public class AccountResponse
    {
        public GroupHeaders group_header { get; set; }
        public Response response { get; set; }
    }
        public class GroupHeaders
        {
            public string message_identification { get; set; }
            public DateTime creation_date_time { get; set; }
            public int http_code { get; set; }
        }
        public class Response
        {
            public List<Account> accounts { get; set; }
            public List<Link> _links { get; set; }
        }

    public class Link
        {
            public string rel { get; set; }
            public string href { get; set; }
        }

        public class Account
        {
            public string _id { get; set; }
            public string country { get; set; }
            public string iban { get; set; }
            public string bban { get; set; }
            public string currency { get; set; }
            public string account_name { get; set; }
            public string product { get; set; }
            public string account_type { get; set; }
            public string bic { get; set; }
            public string status { get; set; }
            public string credit_limit { get; set; }
            public string available_balance { get; set; }
            public string booked_balance { get; set; }
            public string value_dated_balance { get; set; }
            public string latest_transaction_booking_date { get; set; }
            public List<Link> _links { get; set; }
            public string plusgiro { get; set; }
            public string account_short_name { get; set; }
            public string group_account_type { get; set; }
            public string legal_balance { get; set; }
            public string master_account_number { get; set; }
            public string branch_code { get; set; }
        }
}
